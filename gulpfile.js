var gulp = require("gulp"),
	// server = require("gulp-server-livereload"),
	concat = require("gulp-concat"),
	uglify = require("gulp-uglify"),
	// minifyCss = require("gulp-minify-css"),
	sourcemaps = require("gulp-sourcemaps"),
	rename = require("gulp-rename"),
	uglifycss = require('gulp-uglifycss'),
	autoprefixer = require("gulp-autoprefixer"),
	prettyData = require("gulp-pretty-data");

// App js
gulp.task("js", function () {
	return gulp.src([
			"js/src/*.js",
			"!js/src/*.test.js",
			"!js/src/skip*.js"
		])
		.pipe(sourcemaps.init())
		.pipe(concat("app.concat.js"))
		.pipe(uglify({
			mangle: false
		}))
		.pipe(rename("app.min.js"))
		.pipe(sourcemaps.write("./"))
		.pipe(gulp.dest("js/"));
});

// Test js
// gulp.task("tests", function () {
// 	return gulp.src([
// 			"!js/src/skip*.js",
// 			"js/src/*.test.js"
// 		])
// 		.pipe(concat("tests.min.js"))
// 		.pipe(uglify({
// 			mangle: false
// 		}))
// 		.pipe(gulp.dest("js/"));
// });

// Обфускатор для krpano action
function obfuscateAction() {
	function dry (file, cb) {
		// console.log(String(file.contents).match(/(<action.+?>)([\s\S]+)(<\/action>)/i));
		file.contents = new Buffer(String(file.contents).replace(/(<action.+?>)([\s\S]+?)(<\/action>)/ig, function (str, opentag, cnt, closetag) {
			return opentag + cnt.replace(/\s*(^|[;,=!])\s*/g, "$1") + closetag;
		}));
		cb(null, file);
	}

	return require('event-stream').map(dry);
}

// Минификатор для включаемых xml krpano
gulp.task("xml", function () {
	return gulp.src("pano/src/*.xml")
		.pipe(prettyData({type: 'minify'}))
		.pipe(obfuscateAction())
		.pipe(gulp.dest("pano/"));
});

// App css
gulp.task("css", function () {
	return gulp.src([
		"css/src/*.css",
		"bower_components/leaflet/dist/leaflet.css",
		"!css/src/skip*.css"
	])
	.pipe(concat("app.min.css"))
	.pipe(autoprefixer({
		browsers: ["last 4 versions"],
		cascade: false
	}))
	.pipe(uglifycss())
	.pipe(gulp.dest("css/"));
});

// Watch
gulp.task("watch", function () {
	gulp.watch("js/src/*.js", ["js"]);
	// gulp.watch("js/src/*.test.js", ["tests"]);
	gulp.watch("css/src/*.css", ["css"]);
	gulp.watch("pano/src/*.xml", ["xml"]);
});

// Manual lib js
gulp.task("lib-js-uglify", function () {
	return gulp.src([
			"bower_components/SlimClamp/slimclamp.js"
		])
		.pipe(uglify({
			mangle: false
		}))
		.pipe(gulp.dest(".tmp/"));
});
gulp.task("lib-js-concat", function () {
	return gulp.src([
			"bower_components/jquery/dist/jquery.min.js",
			"bower_components/leaflet/dist/leaflet.js",
			".tmp/slimclamp.js",
			"js/vendor/pano.js",
			// "bower_components/underscore/underscore-min.js",
			// "bower_components/jquery-ui/ui/minified/core.min.js",
			// "bower_components/jquery-ui/ui/minified/widget.min.js",
			// "bower_components/jquery-ui/ui/minified/mouse.min.js",
			// "bower_components/jquery-ui/ui/minified/position.min.js",
			// "bower_components/jquery-ui/ui/minified/draggable.min.js",
			// "bower_components/jquery-ui/ui/minified/resizable.min.js",
			// "bower_components/jquery-ui/ui/minified/button.min.js",
			// "bower_components/jquery-ui/ui/minified/menu.min.js",
			// "bower_components/jquery-ui/ui/minified/effect.min.js",
			// "bower_components/jquery-ui/ui/minified/effect-fade.min.js",
			// "bower_components/jquery-ui/ui/minified/datepicker.min.js",
			// "bower_components/jquery-ui/ui/minified/slider.min.js",
			// "bower_components/jquery-ui/ui/minified/autocomplete.min.js",
			// "bower_components/jquery-ui/ui/minified/dialog.min.js",
			// "bower_components/jquery-ui/ui/minified/i18n/datepicker-ru.min.js",
			// "bower_components/jquery-placeholder/jquery.placeholder.min.js",
			// "bower_components/angular/angular.min.js",
			// "bower_components/autosize/dist/autosize.min.js",
			// "bower_components/angular-ui-router/release/angular-ui-router.min.js",
			// "bower_components/fotorama/fotorama.js",
			// "bower_components/imagesloaded/imagesloaded.pkgd.min.js",
			// "bower_components/masonry/dist/masonry.pkgd.min.js"
		])
		.pipe(concat("lib.min.js"))
		.pipe(gulp.dest("js/"));
});
gulp.task('lib-js', ['lib-js-uglify', 'lib-js-concat']);

// Manual dev js
// gulp.task("dev-js", function() {
// 	return gulp.src([
// 			"bower_components/jasmine/lib/jasmine-core/jasmine.js",
// 			"bower_components/jasmine/lib/jasmine-core/jasmine-html.js",
// 			"bower_components/jasmine/lib/jasmine-core/boot.js",
// 			"bower_components/angular-mocks/angular-mocks.js",
// 			"bower_components/testwood/testwood.js",
// 		])
// 		.pipe(concat("dev.min.js"))
// 		.pipe(gulp.dest("js/"));
// });

// Manual dev css
// gulp.task("dev-css", function() {
// 	return gulp.src([
// 			"bower_components/jasmine/lib/jasmine-core/jasmine.css",
// 			"bower_components/testwood/testwood.css"
// 		])
// 		.pipe(concat("dev.min.css"))
// 		.pipe(gulp.dest("css/"));
// });


/*	Command-line API
 */

// "$ gulp"
// gulp.task("default", ["webserver", "watch"]);
gulp.task("default", ["watch"]);

gulp.task('make', ['lib-js', 'js', 'css', 'xml']);
// "$ gulp manual"
// gulp.task("manual", ["dev-js", "dev-css", "lib-js"]);