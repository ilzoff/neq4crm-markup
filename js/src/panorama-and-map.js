
//
// Панорама и Карта вместе верные друзья.
//

(function (global) {
	var krp, h, v, newH, newV, newHF, hl, hr, newHL, newHR, point, newPoint,
		$map, $markerSelected, $leftLine, $rightLine,
		// o -- это точки, которые изображают стрелку в плане
		// 0 точка —— это впадина в хвосте
		// ось X по направлению острия
		// o = [[0, 40], [-9, 35], [0, 57], [9, 35]],
		// c = [0, 150 * Math.cos(v * L.LatLng.DEG_TO_RAD)],
		// c = [0, 180],		// координаты центра поворота всех стрелок и их (0, 0) относительно центра сферы, но ось X смотрит по направлению взгляда
		// posv = v + (vf / 3.0) * Math.cos(v * L.LatLng.DEG_TO_RAD),
		// posv = 16.0,		// сферический угол тангажа оси X для точки C (центр вращение стрелок ниже горизонта)
		d1 = 11, d2 = 45,	// расстояние в пикселях от центра маркера на карте до начала и конца линии сектора обзора
		angleThreshold = 0.0001,
		xmlToLatLng = {},
		xmlToMarker = {},
		xmlToNumber = {},
		numberToXml = {},
		markerSelectedIcon = L.divIcon({
			className: 'marker-selected',
			iconSize: L.point(14, 14),
			iconAnchor: L.point(7, 7)
		}),
		colorNonOrdered = '#999999',
		colorDiffBorder = -0x66,
		hashFire = true;

	// Объект krpano, к которому можно обращаться для управления панорамой.
	function initPanoramaObject() {
		if (!krp) {
			krp = document.getElementById('krpanoSWFObject');
		}
		if (krp.call) {
			if (global.SHOW_KRPANO_CONSOLE) {
				krp.call('showlog();');
			}
			return krp;
		} else {
			return null;
		}
	}

	// Проверяем если ли на странице заготовка для панорамы <панорама id="pano">
	// Если да, вставляем её туда
	function initPanorama() {
		var panorama = $('#pano');
		if (panorama.length > 0) {
			var xml = panorama.attr('data-xml') ? panorama.attr('data-xml') : 'pano.xml',
				swf = panorama.attr('data-swf') ? panorama.attr('data-swf') : 'pano.swf',
				hlookat = panorama.attr('data-hlookat') ? parseInt(panorama.attr('data-hlookat')) : 0,
				fov = panorama.attr('data-fov') ? parseInt(panorama.attr('data-fov')) : 60,
				vars = {},
				hash = (typeof sessionStorage.hash === 'undefined') ? document.location.hash.replace('#', '') : sessionStorage.hash.replace('#', '');
			delete sessionStorage.hash;
			if (/\/.+\.xml$/.test(hash)) {
				xml = hash;
			} else {
				var number = parseInt(hash);
				if (!isNaN(number) && number > 0) {
					xml = numberToXml[number];
				}
			}
			vars['view.hlookat'] = hlookat;
			vars['view.fov'] = fov;
			vars['view.vlookat'] = 0;
			embedpano({
				swf: swf,
				xml: xml,
				target: 'pano',
				html5: 'prefer',
				vars: vars
			});
			$('zoom-in').click(function () {
				zoomPanorama(-25);
			});
			$('zoom-out').click(function () {
				zoomPanorama(25);
			});
		}
	}

	// Рисуем линии на карте, отвечающие правому и левому краю изображения.
	// Проецируем их из гео-координат в пиксели, а потом обратно, превращая их в L.polyline
	function viewChanged() {
		if (!initPanoramaObject() || (typeof $markerSelected === 'undefined')) {
			return false;
		}
		newH = Number(krp.get('view.hlookat'));
		newHF = Number(krp.get('view.hfov'));
		if (isNaN(newH) || isNaN(newHF)) {
			return false;
		}
		newHL = newH - newHF / 2.0;
		newHR = newH + newHF / 2.0;
		newPoint = $map.project($markerSelected.getLatLng());
		if (typeof hl === 'number' && typeof hr === 'number' &&
			Math.abs(newHL - hl) < angleThreshold && Math.abs(newHR - hr) < angleThreshold &&
			newPoint.equals(point)) {
			return false;
		}
		hl = newHL;
		hr = newHR;
		point = newPoint;
		var l1 = point.add(L.point(d1 * Math.sin(hl * L.LatLng.DEG_TO_RAD), -d1 * Math.cos(hl * L.LatLng.DEG_TO_RAD))),
			l2 = point.add(L.point(d2 * Math.sin(hl * L.LatLng.DEG_TO_RAD), -d2 * Math.cos(hl * L.LatLng.DEG_TO_RAD))),
			r1 = point.add(L.point(d1 * Math.sin(hr * L.LatLng.DEG_TO_RAD), -d1 * Math.cos(hr * L.LatLng.DEG_TO_RAD))),
			r2 = point.add(L.point(d2 * Math.sin(hr * L.LatLng.DEG_TO_RAD), -d2 * Math.cos(hr * L.LatLng.DEG_TO_RAD))),
			llll = [$map.unproject(l1), $map.unproject(l2)],
			rlll = [$map.unproject(r1), $map.unproject(r2)];
		if (typeof $leftLine === 'undefined') {
			$leftLine = L.polyline(llll, {color: '#000000', weight: '1', });
			$map.addLayer($leftLine);
		} else {
			$leftLine.setLatLngs(llll);
		}
		if (typeof $rightLine === 'undefined') {
			$rightLine = L.polyline(rlll, {color: '#000000', weight: '1', });
			$map.addLayer($rightLine);
		} else {
			$rightLine.setLatLngs(rlll);
		}
	}

	// Плавное приближение или удаление панорамы. Аргумент -- разница угла обзора до и после анимации.
	// Положительный аргумент делает угол обзора больше -- следовательно, отдаляет изображение.
	function zoomPanorama(inc) {
		if (!initPanoramaObject()) {
			return false;
		}
		var fov = Number(krp.get('view.fov'));
		fov = fov * (100.0 + inc) / 100.0;
		krp.call('tween(view.fov, ' + fov + ', 0.5, easeOutInCubic)');
		return true;
	}

	// Для обрезания цвета чтобы он никогда не превышал 255 и не спускался ниже 0.
	function trim_255(c) {
		return c < 255 ? c < 1 ? 0 : c : 255;
	}

	// Изменение цвета на разницу diff если положительный, то ярче, если отрицательный, то темнее.
	function colorLightenDarken(clr, diff) {
		var num = parseInt(clr.replace('#', ''), 16),
			r = (num >> 16) + Math.round(diff),
			g = (num >> 8 & 0xff) + Math.round(diff),
			b = (num & 0xff) + Math.round(diff);
		return '#' + (0x1000000 + (trim_255(r) << 16) + (trim_255(g) << 8) + trim_255(b)).toString(16).slice(1);
	}

	// Конструируем стиль маркера из приехавших в GeoJSON параметров. В частности состояния заказа и цвета.
	function markerStyleFromProperties(props) {
		var style = {
			radius: 5,
			weight: 1,
			opacity: 1,
			fillOpacity: 1
		};
		style.fillColor = props.ordered ? props.color : colorNonOrdered;
		style.color = colorLightenDarken(style.fillColor, colorDiffBorder);
		return style;
	}

	// Инициализация карты. Ищем эелемент <карта id="map">, грузим данные по точкам и связям из урла в аттирбутах.
	function initMap() {
		var map = $('#map');
		if (map.length === 0) {
			return;
		}
		var jsonUrl = map.attr('data-geo-json-url') ? map.attr('data-geo-json-url') : 'rigaland.json';
		$map = new L.map("map", {
									zoomControl: false,
									fadeAnimation: false,
									inertia: false,
									zoomAnimation: false,
									markerZoomAnimation: false,
									keyboard: false
								});
		var yndx = new L.Yandex(),
			satt = new L.Yandex('satellite');
		$map.addLayer(yndx);

		map.on("map-style-change", function (event, newStyle) {
			switch(newStyle) {
				case 'map':
					if ($map.hasLayer(satt)) {
						$map.addLayer(yndx);
						$map.removeLayer(satt);
					}
					break;
				case 'satellite':
					if ($map.hasLayer(yndx)) {
						$map.addLayer(satt);
						$map.removeLayer(yndx);
					}
					break;
			}
		});

		$('panorama-info').on('panorama-order-change', function (event, ordered) {
			var xmlName = krp.get('xml.url');
			if (!xmlToMarker.hasOwnProperty(xmlName)) {
				return false;
			}
			xmlToMarker[xmlName].feature.properties.ordered = ordered;
			xmlToMarker[xmlName].setStyle(markerStyleFromProperties(xmlToMarker[xmlName].feature.properties));
		});

		$.ajax({
			type: 'GET',
			dataType: 'json',
			url: jsonUrl,
			success: function (data) {
				var layer = L.geoJson(data, {
					pointToLayer: function (feature, latlng) {
						return L.circleMarker(latlng, markerStyleFromProperties(feature.properties));
					},
					onEachFeature: function (feature, layer) {
						// Не вешать обработчики на связи, а только на маркеры (у них есть properties внутри geoJson)
						if (!feature.properties) {
							layer.options.clickable = false;
							layer.setStyle({
								color: '#cc3333',
								weight: 1,
								opacity: 1,
							});
							return;
						}
						xmlToLatLng[feature.properties.xml] = L.latLng(feature.geometry.coordinates.slice(0,2).reverse());
						xmlToMarker[feature.properties.xml] = layer;
						xmlToNumber[feature.properties.xml] = feature.properties.number;
						numberToXml[feature.properties.number] = feature.properties.xml;
						layer.on({
							click: onMarkerClick,
							mouseover: onMarkerMouseOver,
							mouseout: onMarkerMouseOut
						});
					}
				});
				$map.addLayer(layer);
				$map.fitBounds(layer.getBounds());
				$map.setMaxBounds($map.getBounds().pad(0.1));
				$map.options.minZoom = $map.getZoom() - 1;
				initPanorama();
			},
			error: function (jqXHR, textStatus, errorThrown) {
				console.log(errorThrown);
			}
		});
	}

	// Чтобы не создавать функции в цикле, испольуем одну и ту же функцию.
	// При нажатии на макрер-круг открыть соответствующую панораму.
	function onMarkerClick(e) {
		if (!initPanoramaObject) {
			return false;
		}
		krp.call('loadpano(' + e.target.feature.properties.xml + ', null, KEEPVIEW, BLEND(0.2))');
	}

	// Чтобы не создавать функции в цикле, испольуем одну и ту же функцию.
	// Открываем подсказку с уидом точки при наведении.
	function onMarkerMouseOver(e) {
		var popup = new L.popup({offset: new L.point(0, -6), minWidth: 0})
						.setLatLng(e.target.feature.geometry.coordinates.slice(0,2).reverse())
						.setContent('№' + e.target.feature.properties.number.toString())
						.openOn($map);
	}

	// Чтобы не создавать функции в цикле, испольуем одну и ту же функцию.
	// Закрываем подсказку с уидом точки при уводе мыши.
	// TODO: Нужно как-то более френдли обходиться с попапом, чтобы можно было текст скопировать?
	function onMarkerMouseOut(e) {
		$map.closePopup();
	}

	// Эта функция запускается при каждой смене сцен и при загрузке geoJson (асинхронный джаваскрипт асинхронен),
	// чтобы избежать проблемы, когда панорама загружена, а маркеры — нет.
	function panoLoadComplete() {
		if (!initPanoramaObject()) {
			return false;
		}
		var xmlName = String(krp.get('xml.url')),
			latlng = xmlToLatLng[xmlName];
		if (typeof latlng === 'undefined') {
			return false;
		}
		if (typeof $markerSelected === 'undefined') {
			$markerSelected = L.marker(latlng, {icon: markerSelectedIcon, clickable: false}).addTo($map);
		} else {
			$markerSelected.setLatLng(latlng);
		}
		if (xmlToMarker.hasOwnProperty(xmlName)) {
			hashFire = false;
			document.location.hash = xmlToNumber[xmlName];
			$('panorama-info number').text(xmlToMarker[xmlName].feature.properties.number);
			toggleOrdered(xmlToMarker[xmlName].feature.properties.ordered);
		}
	}

	function getCurrentPanoramaUID() {
		if (!initPanoramaObject()) {
			return false;
		}
		var xmlName = String(krp.get('xml.url'));
		if (!xmlToMarker.hasOwnProperty(xmlName)) {
			return false;
		}
		return xmlToMarker[xmlName].feature.properties.uid;
	}

	// global.updateArrow = updateArrow;
	global.viewChanged = viewChanged;
	global.panoLoadComplete = panoLoadComplete;
	global.getCurrentPanoramaUID = getCurrentPanoramaUID;

	// Инициализация при загрузке документа.
	$(function () {
		initMap();
		$(window).on('hashchange', function () {
			if (!hashFire) {
				hashFire = true;
				return false;
			}
			var xml, hash = document.location.hash.replace('#', '');
			if (/\/.+\.xml$/.test(hash)) {
				xml = hash;
			} else {
				var number = parseInt(hash);
				if (!isNaN(number) && number > 0) {
					xml = numberToXml[number];
				}
			}
			if (!initPanoramaObject || typeof xml === 'undefined') {
				return false;
			}
			krp.call('loadpano(' + xml + ', null, KEEPVIEW, BLEND(0.2))');
		});
	});
}(this));
