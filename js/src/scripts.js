//
//	Переключение "страниц" (список объектов vs панорама)
//

// ;(function () {
// 	if (location.hash.search("#panorama-page") >= 0) {
// 		$(document).on("ready", function () {
// 			switchPage("#panorama-page");
// 		});
// 	} else {
// 		$(document).on("ready", function () {
// 			switchPage("#list-page");
// 		});
// 	}

// 	$(document).on("click", "[switch-page]", function (event) {
// 		var page = $(this).attr("href").split("#")[1];
// 		switchPage("#"+page);
// 	});

// 	$(window).on("popstate", function (event) {
// 		if (location.hash.search("#panorama-page") >= 0) {
// 			switchPage("#panorama-page");
// 		} else {
// 			switchPage("#list-page");
// 		}
// 	});
// }());

// function switchPage(id) {
// 	$("page").attr("hidden", "hidden");
// 	if ($(id).length > 0) {
// 		$(id).removeAttr("hidden");
// 	} else {
// 		$("page").eq(0).removeAttr("hidden");
// 	}
// 	setTimeout(function () {
// 		$("html, body").scrollTop(0)
// 	},10);
// }


//
// Вынимаем SVG
//

;(function () {
	var selector = "img[data-extract-svg]";
	$(document).on("ready", function () {
		extractSVG($(selector));
	});
}());

function extractSVG ($images) {
	$images.each(function () {
		var image = $(this);
		$.get(image.attr("src"), function (contents) {
			var svg = $(contents).find("svg");
			svg.attr("class", image.attr("class"));
			svg.attr("id", image.attr("id"));
			image.after(svg);
			image.remove();
		});
	});
}

//
// Выбор текущей панорамы из дропдауна
//

;(function () {
	$(document).on("click", ".panorama-list-dropdown dropdown-arrow", function () {
		togglePanoramaDropdown();
	});

	// Заморочка чтоб стрелочку сделать белой
	$(document).on("mouseenter", ".panorama-list-dropdown .panorama-list li", function () {
		if ($(this).prev().length === 0) {
			if (!$(this).hasClass("active")) {
				$(".panorama-list-dropdown dropdown-arrow").addClass("white");
			}
		}
	});
	$(document).on("mouseleave", ".panorama-list-dropdown .panorama-list li", function () {
		if ($(this).prev().length === 0) {
			$(".panorama-list-dropdown dropdown-arrow").removeClass("white");
		}
	});

	// Выбираем
	$(document).on("click", ".panorama-list-dropdown .panorama-list a", function () {
		var li = $(this).parents("li").eq(0);
		li.siblings(".active").removeClass("active");
		li.addClass("active");
		togglePanoramaDropdown(false);
	});
}());

function togglePanoramaDropdown(force) {
	var d = $(".panorama-list-dropdown");
	d.find(".panorama-list").toggleClass("active", force);
	d.find("dropdown-arrow").toggleClass("active", force);
}

//
// Фулскрин
//

;(function () {
	$(document).on("click", "fullscreen", function () {
		$("body, panorama, .panorama-placeholder-wrapper").toggleClass("fullscreen");
	});
}());

//
// Тултип
//

;(function () {
	// var tooltipOn = false;
	$(document).on("click", "[toggle-tooltip]", function (event) {
		var self = $(this);
		var tooltip = $(self.attr("href"));
		if (tooltip.length > 0) {
			var pos = self.position();
			tooltip.css({
				top: pos.top + "px",
				left: (pos.left + self.width()*0.5) + "px",
			});
			if (tooltip.attr("hidden")) {
				tooltip.removeAttr("hidden");
				// tooltipOn = true;
				$(document).on("click", checkHideTooltip);
			} else {
				tooltip.attr("hidden", "hidden");
				// tooltipOn = false;
				$(document).off("click", checkHideTooltip);
			}

		}
		event.preventDefault();
	});

	function checkHideTooltip (event) {
		var target = $(event.target);
		console.log(target.is("*[toggle-tooltip]"), target.parents("*[toggle-tooltip]").length < 1);
		if (!target.is("*[toggle-tooltip]") && target.parents("*[toggle-tooltip]").length < 1 && target.parents("*[tooltip]").length < 1 && !target.is("*[tooltip]")) {
			$("*[tooltip]").attr("hidden", "hidden");
			// tooltipOn = true;
		}
	}
}());

//
// Добавить в заказ
//

function toggleOrdered(ord) {
	var info = $('panorama-info'),
		panorama = $('panorama'),
		ordered = (typeof ord === 'undefined') ? !info.get(0).hasAttribute("ordered") : ord;
	if (ordered) {
		if (!info.get(0).hasAttribute("ordered")) {
			info.attr('ordered', 'true');
			info.trigger("panorama-order-change", ordered);
			panorama.trigger("panorama-order-change", ordered);
		}
	} else if (info.get(0).hasAttribute("ordered")) {
		info.removeAttr('ordered');
		info.trigger("panorama-order-change", ordered);
		panorama.trigger("panorama-order-change", ordered);
	}
	return ordered;
}

// ;(function () {
	$(document).on("click", "panorama-info a[toggle-order]", function (e) {
		e.preventDefault();
		var orderObj = {
			uid: getCurrentPanoramaUID(),
			ordered: toggleOrdered()
		};
		orderObj[$('meta[name="csrf-param"]').attr('content')] = $('meta[name="csrf-token"]').attr('content');
		$.ajax({
			type: 'POST',
			dataType: 'json',
			contentType: 'application/json',
			url: $('panorama-info').attr('data-order-url'),
			data: JSON.stringify(orderObj),
			success: function (data) {
				$('due').text(data.due);
				for (var dt in data.quantity) {
					$('p[data-shooting-date-type="' + dt + '"] quantity').text(data.quantity[dt]);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				console.log(errorThrown);
			}
		});
	});
// }());

//
// Исчезающий текст "добавлено в заказ"
//

$(document).on("ready", function () {
	$('panorama').on("panorama-order-change", function (event, order) {
		var pano = $(this),
			selector = ".order-change-true",
			template = $("<div class='order-change-true'></div>");
		if (order) {
			pano.siblings(selector).remove();
			template.clone().insertAfter(pano);
			setTimeout(function () {
				pano.siblings(selector).addClass("add");
			}, 1);
			setTimeout(function () {
				pano.siblings(selector).addClass("remove").removeClass("add");
			}, 100)
			setTimeout(function () {
				pano.siblings(selector).remove();
			}, 600);
			// pano.append(template.clone());
		} else {
			pano.siblings(selector).remove();
		}
	});
});

//
// Формы ввода
//


$(document).on("ready", function () {

	// Если поля пустые (при первой загрузке), проставить класс чтоб лейбл
	// правильно отобразился
	$(".field-input").each(function () {
		checkAndSetEmptyField($(this));
	});

	// При вводе чего-либо в поля, тоже следить за пустотой поля
	$(document).on("keyup", ".field-input", function () {
		checkAndSetEmptyField($(this));
	});

	// Хак - перенос класса error из input в родительский контейнер
	// (сервер умеет проставлять только input'у класс)
	$(".field-input input").each(function () {
		if ($(this).hasClass( "error" )) {
			$(this).parents(".field-input").addClass("error");
		}
	});
});


function checkAndSetEmptyField (field) {
	var input = field.find("input");
	if (input.length < 1) {
		return;
	}
	if (!input.val()) {
		field.addClass("empty");
	} else {
		field.removeClass("empty");
	}
}

// Переключение видимости пароля
$(document).on("click", "toggle-input-type", function () {
	var toggle = $(this),
		input = toggle.parents(".field-input").find("input");
	toggle.toggleClass("open");
	input.attr("type", (input.attr("type") == "text" ? "password" : "text"));
});

//
// Валидация
//

// ;(function () {
// 	$(document).on("keyup", "*[validate]", function () {
// 		var form = $(this).parents("form");
// 		// 	field = $(this).parents(".field-input");
// 		// if (field.length > 0) {
// 			var result = validField($(this));
// 			if (result) {
// 				// form.find("input[type='submit'][disabled]").removeAttr("disabled");
// 			} else {
// 				// form.find("input[type='submit']").attr("disabled", "disabled");
// 			}
// 		// }
// 	});
// }());


function attachValidateOnKeyup (form) {
	$(document).on("keyup", "*[validate]", function () {
		validForm(form);
		// var result = validForm(form);
		// if (result) {
		// 	form.find("input[type='submit'][disabled]").removeAttr("disabled");
		// } else {
		// 	form.find("input[type='submit']").attr("disabled", "disabled");
		// }
	});
}

function validForm (form, silentError) {
	var fields = form.find("*[validate]"),
		result = 1;
	fields.each(function () {
		result = result * validField($(this), silentError);
	});
	return (result == 0 ? false : true);
}

function validField (field, silentError) {
	var checkType = field.attr("validate"),
		result = 1;
	if (checkType.match(/required/)) {
		result = result * checkRequiredField(field, silentError);
	}
	if (checkType.match(/email/)) {
		result = result * checkEmailField(field, silentError);
	}
	if (checkType.match(/match/)) {
		result = result * checkMatchField(field, silentError);
	}
	if (checkType.match(/minlength/)) {
		result = result * checkMinlengthField(field, silentError);
	}
	return result;
}

function checkRequiredField (field, silentError) {
	var val = field.val();
	if (!val && !silentError) {
		// console.log("required error", val);
		setError(field, "required");
		return 0;
	} else {
		// console.log("required valid", val);
		clearError(field, "required");
		return 1;
	}
}

function checkEmailField(field, silentError) {
	var val = field.val(),
		r1 = /^(?!\.)((?!.*\.{2})[a-zA-Z0-9\u0080-\u00FF\u0100-\u017F\u0180-\u024F\u0250-\u02AF\u0300-\u036F\u0370-\u03FF\u0400-\u04FF\u0500-\u052F\u0530-\u058F\u0590-\u05FF\u0600-\u06FF\u0700-\u074F\u0750-\u077F\u0780-\u07BF\u07C0-\u07FF\u0900-\u097F\u0980-\u09FF\u0A00-\u0A7F\u0A80-\u0AFF\u0B00-\u0B7F\u0B80-\u0BFF\u0C00-\u0C7F\u0C80-\u0CFF\u0D00-\u0D7F\u0D80-\u0DFF\u0E00-\u0E7F\u0E80-\u0EFF\u0F00-\u0FFF\u1000-\u109F\u10A0-\u10FF\u1100-\u11FF\u1200-\u137F\u1380-\u139F\u13A0-\u13FF\u1400-\u167F\u1680-\u169F\u16A0-\u16FF\u1700-\u171F\u1720-\u173F\u1740-\u175F\u1760-\u177F\u1780-\u17FF\u1800-\u18AF\u1900-\u194F\u1950-\u197F\u1980-\u19DF\u19E0-\u19FF\u1A00-\u1A1F\u1B00-\u1B7F\u1D00-\u1D7F\u1D80-\u1DBF\u1DC0-\u1DFF\u1E00-\u1EFF\u1F00-\u1FFFu20D0-\u20FF\u2100-\u214F\u2C00-\u2C5F\u2C60-\u2C7F\u2C80-\u2CFF\u2D00-\u2D2F\u2D30-\u2D7F\u2D80-\u2DDF\u2F00-\u2FDF\u2FF0-\u2FFF\u3040-\u309F\u30A0-\u30FF\u3100-\u312F\u3130-\u318F\u3190-\u319F\u31C0-\u31EF\u31F0-\u31FF\u3200-\u32FF\u3300-\u33FF\u3400-\u4DBF\u4DC0-\u4DFF\u4E00-\u9FFF\uA000-\uA48F\uA490-\uA4CF\uA700-\uA71F\uA800-\uA82F\uA840-\uA87F\uAC00-\uD7AF\uF900-\uFAFF\.!#$%&'*+-\/=?^_`{|}~\-\d]+)@(?!\.)([a-zA-Z0-9\u0080-\u00FF\u0100-\u017F\u0180-\u024F\u0250-\u02AF\u0300-\u036F\u0370-\u03FF\u0400-\u04FF\u0500-\u052F\u0530-\u058F\u0590-\u05FF\u0600-\u06FF\u0700-\u074F\u0750-\u077F\u0780-\u07BF\u07C0-\u07FF\u0900-\u097F\u0980-\u09FF\u0A00-\u0A7F\u0A80-\u0AFF\u0B00-\u0B7F\u0B80-\u0BFF\u0C00-\u0C7F\u0C80-\u0CFF\u0D00-\u0D7F\u0D80-\u0DFF\u0E00-\u0E7F\u0E80-\u0EFF\u0F00-\u0FFF\u1000-\u109F\u10A0-\u10FF\u1100-\u11FF\u1200-\u137F\u1380-\u139F\u13A0-\u13FF\u1400-\u167F\u1680-\u169F\u16A0-\u16FF\u1700-\u171F\u1720-\u173F\u1740-\u175F\u1760-\u177F\u1780-\u17FF\u1800-\u18AF\u1900-\u194F\u1950-\u197F\u1980-\u19DF\u19E0-\u19FF\u1A00-\u1A1F\u1B00-\u1B7F\u1D00-\u1D7F\u1D80-\u1DBF\u1DC0-\u1DFF\u1E00-\u1EFF\u1F00-\u1FFF\u20D0-\u20FF\u2100-\u214F\u2C00-\u2C5F\u2C60-\u2C7F\u2C80-\u2CFF\u2D00-\u2D2F\u2D30-\u2D7F\u2D80-\u2DDF\u2F00-\u2FDF\u2FF0-\u2FFF\u3040-\u309F\u30A0-\u30FF\u3100-\u312F\u3130-\u318F\u3190-\u319F\u31C0-\u31EF\u31F0-\u31FF\u3200-\u32FF\u3300-\u33FF\u3400-\u4DBF\u4DC0-\u4DFF\u4E00-\u9FFF\uA000-\uA48F\uA490-\uA4CF\uA700-\uA71F\uA800-\uA82F\uA840-\uA87F\uAC00-\uD7AF\uF900-\uFAFF\-\.\d]+)((\.([a-zA-Z\u0080-\u00FF\u0100-\u017F\u0180-\u024F\u0250-\u02AF\u0300-\u036F\u0370-\u03FF\u0400-\u04FF\u0500-\u052F\u0530-\u058F\u0590-\u05FF\u0600-\u06FF\u0700-\u074F\u0750-\u077F\u0780-\u07BF\u07C0-\u07FF\u0900-\u097F\u0980-\u09FF\u0A00-\u0A7F\u0A80-\u0AFF\u0B00-\u0B7F\u0B80-\u0BFF\u0C00-\u0C7F\u0C80-\u0CFF\u0D00-\u0D7F\u0D80-\u0DFF\u0E00-\u0E7F\u0E80-\u0EFF\u0F00-\u0FFF\u1000-\u109F\u10A0-\u10FF\u1100-\u11FF\u1200-\u137F\u1380-\u139F\u13A0-\u13FF\u1400-\u167F\u1680-\u169F\u16A0-\u16FF\u1700-\u171F\u1720-\u173F\u1740-\u175F\u1760-\u177F\u1780-\u17FF\u1800-\u18AF\u1900-\u194F\u1950-\u197F\u1980-\u19DF\u19E0-\u19FF\u1A00-\u1A1F\u1B00-\u1B7F\u1D00-\u1D7F\u1D80-\u1DBF\u1DC0-\u1DFF\u1E00-\u1EFF\u1F00-\u1FFF\u20D0-\u20FF\u2100-\u214F\u2C00-\u2C5F\u2C60-\u2C7F\u2C80-\u2CFF\u2D00-\u2D2F\u2D30-\u2D7F\u2D80-\u2DDF\u2F00-\u2FDF\u2FF0-\u2FFF\u3040-\u309F\u30A0-\u30FF\u3100-\u312F\u3130-\u318F\u3190-\u319F\u31C0-\u31EF\u31F0-\u31FF\u3200-\u32FF\u3300-\u33FF\u3400-\u4DBF\u4DC0-\u4DFF\u4E00-\u9FFF\uA000-\uA48F\uA490-\uA4CF\uA700-\uA71F\uA800-\uA82F\uA840-\uA87F\uAC00-\uD7AF\uF900-\uFAFF]){2,63})+)$/i;

	console.log(val.match(r1));
	if (!val.match(r1) && !silentError) {
		console.log("email error", val);
		setError(field, "email");
		return 0;
	} else {
		console.log("email valid", val);
		clearError(field, "email");
		return 1;
	}
}

function checkMatchField (field, silentError) {
	var src = $(field.attr("data-match"));
	if (src.length < 1) {
		console.log("неверно указан источник совпадения", field)
		return;
	} else {
		if (field.val() != src.val()) {
			setError(field, "match");
			return 0;
		} else {
			clearError(field, "match");
			return 1;
		}
	}
}

function checkMinlengthField (field, silentError) {
	var min = parseInt(field.attr("data-min-length"));
	if (min || min === 0) {
		if (field.val().length < min) {
			setError(field, "minlength");
			return 0;
		} else {
			clearError(field, "minlength");
			return 1;
		}
	}
}

function setError(field, error) {
	var fieldContainter = field.parents(".field-input");
	fieldContainter.addClass("error error-" + error);
}

function clearError(field, error) {
	var fieldContainter = field.parents(".field-input");
	fieldContainter.removeClass("error-" + error);
	if (!fieldContainter.attr("class").match(/error-/)) {
		fieldContainter.removeClass("error");
	}
}

//
// Переключение типа карты
//

;(function () {
	$(document).on("click", "map-style[type]", function () {
		var mapStyleButton = $(this),
			type = mapStyleButton.attr("type"),
			mapStyleSwitch = mapStyleButton.parents("map-style-switch").eq(0),
			map = mapStyleSwitch.siblings("map").eq(0);
		mapStyleButton.parents("map-style-switch").eq(0).attr("value", type);
		map.trigger("map-style-change", type);
	});
}());

//
// Подрезаем длинные названия
//

;(function () {
	$(document).on("ready", function () {
		$("*[data-lines]").each(function () {
			var element = this,
				lines = $(this).data().lines;
			if ($clamp) {
				$clamp(element, lines);
			}
		});
	})
}());

//
// Переключение боковых панелей-меню на малых экранах
//

;(function () {
	$(document).on("click", "toggle-self", function () {
		var drawer = $(this).parent(),
			drawers = $(".group-wrapper");
		drawer.toggleClass("active");
		drawers.each(function (index,item) {
			if (this != drawer.get(0)) {
				$(this).removeClass("active");
			}
		});

		if ($(".group-wrapper.active").length > 0) {
			$("body").addClass("drawer-over");
		} else {
			$("body").removeClass("drawer-over");
		}
	});

	$(document).on("click", "close-drawer", function () {
		removeDrawers();
	});

}());

function removeDrawers () {
	$(".group-wrapper.active").removeClass("active");
	$("body.drawer-over").removeClass("drawer-over");
}

//
// Хелп
//

function togglePopup(id, force) {
	console.log(id, force);
}
