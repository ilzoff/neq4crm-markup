//
// Переключение попапов
//

;(function () {
	$(document).on("click", "*[toggle-dialog]", function (event) {
		var selector = $(this).attr("href");
		// console.log(selector);
		toggleDialog(selector);
		event.preventDefault();
	});

	$(document).on("click", "overlay", function () {
		$("popup-dialog").each(function () {
			var dialog = $(this);
			if (dialog.hasClass("open")) {
				toggleDialog(dialog, false);
			}
		});
	});
}());


function toggleDialog(selector, force) {
	// Закрыть все диалоги
	if (!selector) {
		dialogToggleBodyStyle(false);
		dialogToggleOverlay(false);
		$("popup-dialog.open").each(function () {
			dialogToggleDialog($(this), false);
		});
	}
	// Переключить конкретный диалог
	else {
		var dialog = $(selector);
		if (dialog.length > 0) {
			if (dialog.get(0) != $("popup-dialog.open").get(0) && $("popup-dialog.open").length > 0 && force === undefined) {
				dialogToggleDialog(dialog, true);
			} else {
				// console.log("force", force);
				dialogToggleBodyStyle(force);
				dialogToggleOverlay(force);
				dialogToggleDialog(dialog, force);
			}
		}
	}
}

function dialogToggleDialog (dialog, force) {
	// console.log("FFF", force);
	if (dialog.hasClass("open") && !force) {
		dialog.fadeOut(400, function () {
			dialog.removeClass("open");
		});
	} else if (!dialog.hasClass("open") && (force === undefined || force === true)) {
		// console.log("close all");
		$("popup-dialog.open").each(function () {
			var _dialog = $(this);
			_dialog.fadeOut(400, function () {
				_dialog.removeClass("open");
			});
		});
		dialog.fadeIn(400, function () {
			dialog.addClass("open");
		});
	}
}

function dialogToggleOverlay (force) {
	// console.log("overlay", force);
	var overlay = $("overlay");

	if (overlay.hasClass("dialog") && !force) {
		overlay.fadeOut(200, function () {
			overlay.removeClass("dialog");
		});
	} else if (!overlay.hasClass("dialog") && (force === undefined || force === true)) {
		overlay.fadeIn(200, function () {
			overlay.addClass("dialog");
		});
	}
}

function dialogToggleBodyStyle (force) {
	// console.log("body", force);
	var body = $(document.body);
	if (body.hasClass("dialog") && !force) {
		var top = Math.abs(parseInt(body.css("top")));
		body.css({
				"position": "",
				"top": "",
				"left": "",
				"overflow-y": "",
				"width": "",
				"height": ""
			})
			.removeClass("dialog")
			.scrollTop(top);
	} else if (!body.hasClass("dialog") && (force === undefined || force === true)) {
		body.css({
				"position": "fixed",
				"top": -1 * body.scrollTop() + "px",
				"left": "0px",
				"overflow-y": "scroll",
				"width": "100%",
				"height": "100%"
			})
			.addClass("dialog");
	}
}
