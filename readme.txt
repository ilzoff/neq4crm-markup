Исходные css и js файлы собираются/минифицируются через gulp и лежат в js/src и css/src. Редактировать поэтому их там, а чтоб врубить автосборщик, нужно:

1. Установить Nodejs
2. Установить bower: 'npm install bower -g'
3. 'npm install'
4. 'bower install'
5. Установить gulp: 'npm install gulp -g'
6. Положить тайлы RIGALAND-... в корень репозитория

Запуск автосборщика 'gulp watch'

Запуск микровебсервера тестового 'node web-server.js'

Библиотеки добавлять через Bower: 'bower install MyFavoriteLib --save'. Не забыть прописать нужные оттуда файлы в gulpfile.js, в задачу "lib-js". Собрать вручную минифицированные библиотеки 'gulp lib-js' и перезапустить автосборщик 'gulp watch'.

(все команды выполнять в корне папки)
